<?php
// The pi() function returns the value of PI
echo pi();
echo "<br>";

// PHP min() and max() Functions
echo(min(0, 150, 30, 20, -8, -200));  // returns -200

echo "<br>";

echo(max(0, 150, 30, 20, -8, -200));  // returns 150

echo "<br>";
// The abs() function returns the absolute (positive) value of a number
echo(abs(-6.7));  // returns 6.7
echo "<br>";

// The round() function rounds a floating-point number to its nearest integer
echo(round(0.60));  // returns 1
echo(round(0.49));  // returns 0

// The rand() function generates a random number
echo(rand());
// if you want a random integer between 10 and 100 (inclusive), use rand(10, 100)
echo(rand(10, 100));