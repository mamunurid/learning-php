<?php
// string count
$x="PHP String";
echo strlen($x);
echo ("<br>");

// word count
echo str_word_count($x);
echo ("<br>");

// array count
$cars=array("Volvo","BMW","Toyota");
echo count($cars);

// Check if the type of a variable is integer
$x = 5985;
var_dump(is_int($x));
echo ("<br>");

$x = 59.85;
var_dump(is_int($x));
echo ("<br>");

// Check if the type of a variable is float
$x = 10.365;
var_dump(is_float($x));
echo "<br>";

// Check if the variable is numeric   
$x = 5985;
var_dump(is_numeric($x));

echo "<br>";

$x = "5985";
var_dump(is_numeric($x));

echo "<br>";

$x = "59.85" + 100;
var_dump(is_numeric($x));

echo "<br>";

$x = "Hello";
var_dump(is_numeric($x));
echo "<br>";

// Cast float to int 
$x = 23465.768;
$int = (int)$x;
echo $int;
  
echo "<br>";

// Cast string to int
$x = "23465.768";
$int = (int)$x;
echo $int;